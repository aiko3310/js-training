
var sendit = document.querySelector('.sendit');
sendit.addEventListener('click', pushit, false);
var login = document.querySelector('.login');
login.addEventListener('click',loginto,false);


function pushit(e) {
    e.preventDefault();
    var email = document.querySelector('.account').value;
    var password = document.querySelector('.password').value;
    if (!email) {
        alert('請輸入帳號');
        return;
    }
    if (!password) {
        alert('請輸入密碼');
        return;
    };
    var account = {};
    account.email = email;
    account.password = password;
    var xhr = new XMLHttpRequest();
    xhr.open('post', 'https://hexschool-tutorial.herokuapp.com/api/signup', true);
    xhr.setRequestHeader("Content-type", "application/json");

    var senddata = JSON.stringify(account);
    xhr.send(senddata);
    xhr.onload = function () {
        var callbackdata = JSON.parse(xhr.responseText);
        var veriStr = callbackdata.message;
        if (veriStr == '帳號註冊成功') {
            alert('帳號註冊成功');
        } else if (veriStr == '此帳號已被使用') {
            alert('此帳號已被使用')
            document.querySelector('.account').value = null;
            document.querySelector('.password').value = null;
            return;
        } else {
            alert('帳號註冊失敗');
            document.querySelector('.account').value = null;
            document.querySelector('.password').value = null;
            return;
        }

    }
};
function loginto(e) {
    e.preventDefault();
    var loginac = document.querySelector('.loginac').value;
    var loginps = document.querySelector('.loginps').value;

    if (!loginac) {
        alert('請輸入帳號');
        return;
    };
    if (!loginps) {
        alert('請輸入密碼');
        return;
    };
    var accountlogin={};
    accountlogin.email = loginac;
    accountlogin.password = loginps;
    var xhr = new XMLHttpRequest();
    xhr.open('post', 'https://hexschool-tutorial.herokuapp.com/api/signin', true);
    xhr.setRequestHeader("Content-type", "application/json");
    
    var logindata = JSON.stringify(accountlogin);
    xhr.send(logindata);
    xhr.onload = function () {
        var gobackdata = JSON.parse(xhr.responseText);
        var veriiStr = gobackdata.message;
        if (veriiStr == '登入成功') {
            alert('帳號登入成功');
        } else {
            alert('此帳號不存在或帳號密碼錯誤');
            document.querySelector('.loginac').value = null;
            document.querySelector('.loginps').value = null;
            return;
        }

    }
}
