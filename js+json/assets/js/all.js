

var xhr = new XMLHttpRequest;
xhr.open('get', 'https://data.kaohsiung.gov.tw/opendata/DownLoad.aspx?Type=2&CaseNo1=AV&CaseNo2=1&FileType=1&Lang=C&FolderType=', true);
xhr.send(null);

xhr.onload = function () {
    var data = JSON.parse(xhr.responseText);
    var totle = data.length;
    var zoneSelect = document.querySelector('#zoneId');
    var zoneClick = document.querySelector('.zoneclick');
    var zoneChange = document.querySelector('#zoneC');
    var zoneNameChange = document.querySelector('#zoneN')
    zoneSelect.addEventListener('change', zoneShow, false);
    zoneClick.addEventListener('click', zoneShow, false);
    function zoneShow(e) {
        var select = e.target.value;
        var zoneInfo = '';
        for (var i = 0; totle > i; i++) {
            if (select == data[i].Add.match(select)) {
                zoneInfo += '<div class="card col-md-6 px-0">' +
                    '<div class="text-center bg-cover d-flex align-items-end"' +
                    'style=' + '"' + 'background-image:url(' + data[i].Picture1 + ');' + 'width: 100%;height: 150px;' + '">' +
                    '<h5 class="text-light mr-auto pl-3">' + data[i].Name + '</h5>' +
                    '<p class="text-light my-2 pr-3">' + select + '</p></div>' +
                    '<div class="card-body text-left">' +
                    '<ul class="list-unstyled card-text ">' +
                    '<li class="d-flex align-items-center flex-column flex-sm-row"><div>' +
                    '<img src="img/icons_clock.png" class="icons my-2 mr-1">' +
                    data[i].Opentime +
                    '</div></li>' +
                    '<li class="d-flex align-items-center flex-column flex-sm-row"><div>' +
                    '<img src="img/icons_pin.png" class="icons my-2 mr-1">' +
                    data[i].Add +
                    '</div></li>' +
                    '<li class="d-flex align-items-center justify-content-start flex-column flex-sm-row flex-wrap"><div class="mr-sm-auto">' +
                    '<img src="img/icons_phone.png" class="icons my-2 mr-1">' +
                    data[i].Tel +
                    '</div>' +
                    '<div><img src="img/icons_tag.png" alt="" class="icons my-2 mr-1">' +
                    data[i].Ticketinfo +
                    '</div></li>' +

                    '</ul></div></div>'
                    ;
            };
            zoneChange.innerHTML = zoneInfo;
            zoneNameChange.textContent = select;
        };
    };

};